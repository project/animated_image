<?php

/**
 * Implements hook_init().
 */
function animated_image_init() {
  drupal_add_js(drupal_get_path('module', 'animated_image') . '/animated_image.js');
}

/**
 * Implements hook_help().
 */
function animated_image_help($path, $arg) {
  switch ($path) {
    case 'admin/help#animated_image':
      $help = '<p>' . t('Animated image module allow users to convert a set of images stored in a single image into an animation.') . '</p>';
      $help .= '<h2>' . t('Mode of use') . ':</h2>';
      $help .= '<p>' . t('First, you need to join several images into one using some software. <strong>Images must be joined vertically and they must have the same height.</strong>') . '</p>';
      $help .= '<p>' . t('Once you have that image, you can include it using the following code:') . '</p>';
      $help .= '<p>&lt;img class="animated-image" src="' . t('your-image.png') . '" images="5" interval="2" /&gt;</p>';
      $help .= '<p>' . t('The attributes this module can use are:') . '</p>';
      $help .= '<ul><li>' . t('The <strong>images</strong> attribute indicates how many images contains the file you want to animate.') . '</li>';
      $help .= '<li>' . t('The <strong>interval</strong> attribute indicates how many seconds do you want to wait between each frame of the animation. You can use decimal values to make the animation change faster. If this attribute is not defined, 1 second will be used by default.') . '</li></ul>';

      $help .= '<p>' . t('Besides listed attributes you can also define some behaviours by using some classes:') . '</p>';
      $help .= '<ul><li>' . t('<strong>pause-on-click</strong>: allows users to pause and resume the animation by clicking on the image.') . '</li>';
      $help .= '<li>' . t('<strong>paused</strong>: the animation will start paused. You may want to combine this class with the pause-on-click.') . '</li></ul>';

      $help .= '<h2>' . t('Examples') . ':</h2>';
      $help .= '<ul><li>&lt;img class="animated-image" src="' . t('your-image.png') . '" images="5" /&gt;</li>';
      $help .= '<li>&lt;img class="animated-image" src="' . t('your-image.png') . '" images="5" interval="1" /&gt;</li>';
      $help .= '<li>&lt;img class="animated-image pause-on-click" src="' . t('your-image.png') . '" images="5" interval="1" /&gt;</li>';
      $help .= '<li>&lt;img class="animated-image paused pause-on-click" src="' . t('your-image.png') . '" images="5" interval="1" /&gt;</li></ul>';

      $help .= '<h2>' . t('Why images must be joined vertically?') . '</h2>';
      $help .= '<p>' . t('Some themes restrict the width of the page, this affects the dimension of the images and the math used to calculate where is the next image frame located.') . '</p>';

      $help .= '<h2>' . t('How can I join multiple images?') . '</h2>';
      $help .= '<p>' . t('If you want to use an online tool, just search for "join multiple images online". There are a several free services to join images.') . '</p>';

      $help .= '<p>' . t('If you are using Linux, you can use: convert -append myimages*.png result.png') . '</p>';
      return $help;
  }
}
